import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Page from '@atlaskit/page';
import '@atlaskit/css-reset';

import DashboardNavigation from '../components/DashboardNavigation';

export default class App extends Component {

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  render() {
    return (
      <div>
        <Page
          navigationWidth={this.context.navOpenState.width}
          navigation={<DashboardNavigation />}
        >
          {this.props.children}
        </Page>
        <div>
          Stuff
        </div>
      </div>
    );
  }
}
