// @flow

import React, { Component } from 'react';
import styled from 'styled-components';
import Button from '@atlaskit/button';
import DynamicTable from '@atlaskit/dynamic-table';
import ReleaseService, {
  type releaseCommit,
} from '../services/ReleaseService';
import Expander from './Expander';

const Container = styled.div`
  margin-top: 40px;
`;

const Pagination = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Row = styled.div`
  padding-bottom: 16px;
`;

const Changesets = styled.ul`
  padding-bottom: 8px;
`;

type Props = {
  repoOwner: string,
  repoName: string,
}

type State = {
  releaseCommitsPerPage: {
    [pageNumber: string]: releaseCommit[]
  },
  page: number,
  isLoading: boolean,
};

export default class PackageReleaseTable extends Component<Props, State> {
  releaseService;
  rowsPerPage = 10;

  state = {
    releaseCommitsPerPage: {},
    page: 1,
    isLoading: true,
  };

  head = {
    cells: [
      {
        key: 'release',
        content: '',
      },
    ],
  };

  decorateCommit = (commitHash) => {
    const { repoOwner, repoName } = this.props;
    const commitUrl = `https://bitbucket.org/${repoOwner}/${repoName}/commits/${commitHash}`;
    return <a href={commitUrl} target="_blank">{commitHash}</a>;
  }

  renderRow = (row, i) => {
    const date = new Date(row.date);
    const readableDate = `${date.toDateString().substring(0, 3)} ${date.toLocaleString()}`;
    return (
      <Row odd={i % 2}>
        <h4>{readableDate} - {this.decorateCommit(row.commitHash.substr(0, 7))}</h4>
        <Changesets>
          {row.changesetCommits.map( changeset => (
            <li>{this.decorateCommit(changeset.commit)} - {changeset.summary}</li>
          ))}
        </Changesets>
        <Expander>{
          ({ isOpen }) => {
            const packages = row.packages.map(p => `${p.name}@${p.version}`);
            return (
              <span>Packages: {isOpen ? (
                <ul>
                  {packages.map(p => <li>{p}</li>)}
                </ul>
              ) : (
                <span>{ packages.join(', ')}</span>
              )}
              </span>
            );
          }
        }
        </Expander>
      </Row >
    );
  }

  createRows = () => {
    const releaseCommits = this.state.releaseCommitsPerPage[this.state.page];
    return releaseCommits ? releaseCommits.map((release, i) => {
      return {
        key: release.commitHash,
        cells: [
          {
            key: 'release',
            content: this.renderRow(release, i),
          },
        ],
      };
    }) : [];
  };

  async componentDidMount() {
    this.releaseService = new ReleaseService({
      repositoryOwner: this.props.repoOwner,
      repositoryName: this.props.repoName,
    });
    this.changePage(1);
  }

  changePage = async (page) => {
    if (this.state.releaseCommitsPerPage[page]) {
      this.setState({ page });
    } else {
      this.setState({
        isLoading: true,
      });
      const releaseCommits = await this.releaseService.getReleaseCommits(
        'master',
        this.rowsPerPage,
        page,
      );
      this.setState({
        page,
        isLoading: false,
        releaseCommitsPerPage: {
          ...this.state.releaseCommitsPerPage,
          [page]: releaseCommits,
        },
      });
    }
  }

  handlePrevClick = async () => {
    this.changePage(this.state.page - 1);
  }


  handleNextClick = async () => {
    this.changePage(this.state.page + 1);
  }

  render() {
    const rows = this.createRows();
    return (
      <Container>
        <Pagination>
          <Button onClick={this.handlePrevClick} isDisabled={this.state.page === 1}>Previous</Button>
          Page {this.state.page}
          <Button onClick={this.handleNextClick} style={{ float: 'right' }}>Next</Button>
        </Pagination>
        <DynamicTable
          caption=""
          head={this.head}
          rows={rows}
          isFixedSize
          loadingSpinnerSize="large"
          isLoading={this.state.isLoading}
        />
        <Pagination>
          <Button onClick={this.handlePrevClick} isDisabled={this.state.page === 1}>Previous</Button>
          Page {this.state.page}
          <Button onClick={this.handleNextClick} style={{ float: 'right' }}>Next</Button>
        </Pagination>
      </Container>
    );
  }
}
