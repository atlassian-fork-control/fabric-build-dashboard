// @flow

import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';
import PackageReleaseTable from '../components/PackageReleaseTable';

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <h1>Package Releases</h1>
        <PackageReleaseTable repoOwner="atlassian" repoName="atlaskit-mk-2" />
      </ContentWrapper>
    );
  }
}
