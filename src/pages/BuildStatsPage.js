import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';

export default class HomePage extends Component {
  render() {
    return (
      <ContentWrapper>
        <h1>Build Stats</h1>
        <p>Get all your build stats information here.</p>
      </ContentWrapper>
    );
  }
}