// @flow
import parseReleaseCommit from './util/parseReleaseCommit';

type Config = {
  repositoryOwner: string,
  repositoryName: string
};

type Package = {
  name: string,
  version: string
};

export type ReleaseCommit = {
  packages: Package[],
  commitHash: string,
  date: string
};

const getBaseUrl = (repoOwner: string, repoName: string) => {
  return `https://api.bitbucket.org/2.0/repositories/${repoOwner}/${repoName}`;
};

export default class ReleaseService {
  config: Config;
  baseUrl: string;
  releaseCommitsPerPage: {
    [commitPage: number]: number,
  } = {};

  constructor(config: Config) {
    this.config = config;
    this.baseUrl = getBaseUrl(config.repositoryOwner, config.repositoryName);
  }

  getCommitPage = (releaseCommitOffset) => {
    let totalReleaseCommits = 0;
    let commitPage = 1;

    Object.keys(this.releaseCommitsPerPage).find(page => {
      totalReleaseCommits += this.releaseCommitsPerPage[page];
      if (releaseCommitOffset > totalReleaseCommits) {
        commitPage++;
      } else {
        return true;
      }
      return false;
    });
    return commitPage;
  }

  // Get number of release commits for branch starting from releaseCommitPage
  // commitPage is a private arg used to paginate bitbucket commit endpoints rather than release commits
  async getReleaseCommits(
    branch: string,
    number: number = 5,
    releaseCommitPage: number = 1,
    commitPage: number = 1,
  ): Promise<ReleaseCommit[]> {
    const page = commitPage > 1 ? commitPage : this.getCommitPage(number * (releaseCommitPage - 1));
    const commits = await fetch(`${this.baseUrl}/commits/${branch}?page=${page}`)
      .then(res => res.json());
    let releaseCommits = commits.values
      .filter(commit => /^RELEASING:/.test(commit.message))
      .map(commit => {
        const parsedMessage = parseReleaseCommit(commit.message);
        if (!parsedMessage || !parsedMessage.releases) return null;
        return {
          commitHash: commit.hash,
          date: commit.date,
          packages: parsedMessage.releases,
          changesetCommits: parsedMessage.changesets,
        };
      })
      .filter(packages => !!packages);

    this.releaseCommitsPerPage[page] = releaseCommits.length;
    if (releaseCommits.length < number && commits.next) {
      const nextCommits = await this.getReleaseCommits(
        branch,
        number - releaseCommits.length,
        releaseCommitPage,
        page + 1,
      );
      releaseCommits = [...releaseCommits, ...nextCommits];
    }

    return releaseCommits.slice(0, number);
  }
}
